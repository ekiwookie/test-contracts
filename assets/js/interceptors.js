import axios from 'axios';

import store from './store/index.js'
import router from './router.js'

export default function setup() {
  axios.interceptors.request.use(function(config) {
    const token = store.getters['auth/token'];
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
      config.headers.post['Content-Type'] = 'application/json';
      config.headers.common.Accept = 'application/json; charset=utf-8';
    }
    config.baseURL = '/api/';
    return config;
  }.bind(this), function(err) {
    return Promise.reject(err);
  });

  axios.interceptors.response.use(response => {
    return response;
  }, error => {
    if (error.response.status === 401) {
      store.dispatch('auth/logout');
      router.push('/login')
    }
    if (error.response.status === 400) {
      return Promise.reject(error.response);
    }
    return Promise.reject(error.response);
  });
}
