import Vue from 'vue'
import {BootstrapVue, ToastPlugin} from 'bootstrap-vue'

import App from './app.vue'

import router from './router.js'
import store from './store/'
import interceptorsSetup from './interceptors.js'

interceptorsSetup()
Vue.use(BootstrapVue)

const app = new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
