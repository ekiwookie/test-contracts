import axios from "axios";

export default {
  namespaced: true,
  actions: {
    fetchContracts({commit}, data) {
      axios.get('contracts/').then(response => {
        commit('commitContracts', response.data);
      })
    },
    fetchContract({commit}, id) {
      var contract_params = {
        'contract_id': id
      }
      return Promise.all([
        axios.get('contracts/' + id + '/').then(response => {
          commit('commitContract', response.data)
        }),
        axios.get('contract_users/', { 'params': contract_params}).then(response => {
          commit('commitContractUsers', response.data)
        })
      ])
    },
    fetchEditContract({commit}, id) {
      var contract_params = {
        'contract_id': id
      }
      return Promise.all([
        axios.get('available_contract_users/', {'params': contract_params}).then(response => {
          commit('commitAvailableContractUsers', response.data)
        }),
        axios.get('positions/').then(response => {
          commit('commitPositions', response.data)
        }),
      ]);
    },
    destroyContractUser({commit, state}, contract_user_id){
      axios.delete('contract_users/' + contract_user_id +'/').then(response => {
        commit('commitDestroyContractUser', contract_user_id)
      }).catch(error => {
        console.log(error)
      });
    },
    addContractUser({commit, state}, data){
      data.contract_id = state.contract.id;
      return axios.post('contract_users/', data).then(respose => {
        commit('commitAddContractUser', respose.data)
      })
    }
  },
  mutations: {
    commitContracts(state, contracts) {
      state.contracts = contracts
    },
    commitContract(state, contract) {
      state.contract = contract
    },
    commitContractUsers(state, contract_users) {
      state.contract_users = contract_users;
    },
    commitAvailableContractUsers(state, avalable_contract_users) {
      state.avalable_contract_users = avalable_contract_users;
    },
    commitPositions(state, positions) {
      state.positions = positions;
    },
    commitAddContractUser(state, user) {
      state.contract_users.push(user);
    },
    commitDestroyContractUser(state, contract_user_id) {
      var contractUserndex = state.contract_users.findIndex(contractUser => contractUser.id == contract_user_id);
      if (contractUserndex >= 0) {
        state.contract_users.splice(contractUserndex, 1)
      }
    }
  },
  state: {
    contracts: [],
    contract: false,
    contract_users: [],
    avalable_contract_users: [],
    positions: []
  },
  getters: {
    isUserCanManageContract: (state) => (user_id) => {
      var contractUserndex = state.contract_users.find(contractUser => contractUser.user.id == user_id);
      return contractUserndex.position.can_manage_contract_users
    }
  }
}
