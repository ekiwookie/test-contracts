import axios from "axios";

var state = {
  user: false,
  token: false
}
const user = localStorage.getItem('user');
const token = localStorage.getItem('token');
if (user && token) {
  state.token = token;
  try {
    state.user = JSON.parse(user)
  } catch (e) {
    state.token = null;
    state.user = {}
  }
}

export default {
  namespaced: true,
  actions: {
    login({commit}, data) {
      axios.post('token/', {
        username: data.username,
        password: data.password
      }).then((resp) => {
        commit('authSuccess', resp.data);
      }).catch(error => {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
      })
    },
    logout({commit}) {
      commit('logout')
    }
  },
  mutations: {
    authSuccess(state, resp) {
      state.token = resp.access;
      const tokenParsed = JSON.parse(atob(resp.access.split('.')[1]));
      state.user = {
        id: parseInt(tokenParsed.user_id),
        username: tokenParsed.username,
        company: tokenParsed.company,
      }
      localStorage.setItem('token', state.token);
      localStorage.setItem('user', JSON.stringify(state.user));
    },
    logout(state) {
      state.token = null;
      state.user = {};
      localStorage.removeItem('token');
      localStorage.removeItem('user');
    },
  },
  state: state,
  getters: {
    isLoggedIn: state => !!state.token,
    hasPerm: (state) => (checkPerm) => {
      try {
        return JSON.parse(atob(state.token.split('.')[1])).permissions.find(userPerm => userPerm === checkPerm)
      } catch (e) {
        console.log(e);
        return false;
      }
    },
    token: state => state.token,
    userId: state => state.user.id
  }
}
