import Vue from 'vue'
import VueRouter from 'vue-router'

import store from './store/index.js'


import Login from './components/login.vue'
import Contracts from './components/contracts.vue'
import Contract from './components/contract.vue'


Vue.use(VueRouter)

const routes = [
  {
    name: 'contracts',
    path: '/',
    component: Contracts,
    meta: { requiresAuth: true }
  }, {
    name: 'contract',
    path: '/contracts/:id?',
    component: Contract,
    meta: { requiresAuth: true }
  }, {
    name: 'login',
    path: '/login',
    component: Login,
    meta: { requiresNotAuth: true }
  }
]

var router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresEditor)) {
    if(store.getters['auth/hasPerm']('manage_rubrics')){
      next();
      return;
    }
    next('/');
  } else if (to.matched.some(record => record.meta.requiresThemes)) {
    if(store.getters['auth/hasPerm']('manage_themes')){
      next();
      return;
    }
    next('/');
  } else if (to.matched.some(record => record.meta.requiresAdverses)) {
    if(store.getters['auth/hasPerm']('manage_adverses')){
      next();
      return;
    }
    next('/');
  } else if (to.matched.some(record => record.meta.requiresUsers)) {
    if(store.getters['auth/hasPerm']('manage_users')){
      next();
      return;
    }
    next('/');
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if(store.getters['auth/isLoggedIn']){
      next();
      return;
    }
    next('/login');
  } else if (to.matched.some(record => record.meta.requiresNotAuth)) {
    if(!store.getters['auth/isLoggedIn']){
      next();
      return;
    }
    next('/');
  } else {
    next()
  }
})

export default router;
