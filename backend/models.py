from django.db import models
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth import get_user_model


def get_company_string_type_by_content_type(content_type):
    if content_type == ContentType.objects.get_for_model(Organization):
        return 'organization'
    elif content_type == ContentType.objects.get_for_model(Contractor):
        return 'contractor'
    else:
        return False

def content_type_id_by_content_string_type(content_string_type):
    if content_string_type == 'organization':
        return ContentType.objects.get_for_model(Organization)
    elif content_string_type == 'contractor':
        return ContentType.objects.get_for_model(Contractor)


class Organization(models.Model):
    name = models.CharField(
        "Название",
        max_length=100
    )
    attribute1 = models.CharField(
        "Условный атрибут 1",
        max_length=100,
        blank=True,
        null=True,
    )
    attribute2 = models.CharField(
        "Условный атрибут 2",
        max_length=100,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.pk} {self.name}"


class Contractor(models.Model):
    name = models.CharField(
        "Название",
        max_length=100
    )
    attribute3 = models.CharField(
        "Условный атрибут 3",
        max_length=100,
        blank=True,
        null=True,
    )
    attribute4 = models.CharField(
        "Условный атрибут 4",
        max_length=100,
        blank=True,
        null=True,
    )

    def __str__(self):
        return f"{self.pk} {self.name}"


class Contract(models.Model):
    organization = models.ForeignKey(
        Organization,
        verbose_name='Организация',
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )
    contractor = models.ForeignKey(
        Contractor,
        verbose_name='Подрядчик',
        on_delete=models.PROTECT,
        blank=True,
        null=True
    )
    concluded_on = models.DateField(
        verbose_name='Дата заключения',
        default=timezone.now
    )
    def __str__(self):
        return f"{self.pk} {self.concluded_on} {self.organization.name} {self.contractor.name}"


class Position(models.Model):
    name = models.CharField(
        "Название",
        max_length=100
    )
    can_manage_contract_users = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.pk} {self.name}"


class UserCompany(models.Model):
    user = models.OneToOneField(
        get_user_model(),
        related_name='company',
        on_delete=models.PROTECT,
        primary_key=True,
    )
    company_type = models.ForeignKey(
        ContentType,
        on_delete=models.PROTECT,
    )
    company_id = models.PositiveIntegerField()
    company = GenericForeignKey('company_type', 'company_id')

    @property
    def company_type_string(self):
        return get_company_string_type_by_content_type(self.company_type)

    def __str__(self):
        return f"{self.user.username} {self.company.name}"


class ContractUser(models.Model):
    contract = models.ForeignKey(
        Contract,
        related_name='users',
        verbose_name='Контракт',
        on_delete=models.PROTECT,
    )
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.PROTECT,
    )
    position = models.ForeignKey(
        Position,
        on_delete=models.PROTECT,
    )
