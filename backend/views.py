from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.core.exceptions import PermissionDenied
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.response import Response
from rest_framework import viewsets, permissions, status, mixins
from rest_framework.decorators import action
from rest_framework import exceptions

from backend import serializers, models


def index_view(request):
    return render(request, 'index.html')


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['username'] = user.username
        token['company'] = user.company.company.name
        return token


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class PermissionByActionViewSet(viewsets.GenericViewSet):
    permission_classes = (permissions.IsAdminUser,)
    permission_classes_by_action = {}

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, PermissionByActionViewSet):
    queryset = get_user_model().objects.all().order_by('username')
    serializer_class = serializers.UserSerializer

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
    }


class OrganizationViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, PermissionByActionViewSet):
    queryset = models.Organization.objects.all().order_by('name')
    serializer_class = serializers.OrganizationSerializer

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
    }


class ContractorViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, PermissionByActionViewSet):
    queryset = models.Contractor.objects.all().order_by('name')
    serializer_class = serializers.ContractorSerializer

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
    }


class ContractViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, PermissionByActionViewSet):
    serializer_class = serializers.ContractSerializer
    queryset = models.Contract.objects.all()

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
    }

    def get_queryset(self):
        if self.request.user.is_staff:
            queryset = models.Contract.objects.all().order_by('concluded_on')
        else:
            queryset = models.Contract.objects.filter(users__user=self.request.user)

        return queryset


def is_user_can_manage_contract(contract_id, user):
    user_positions_in_contract = models.ContractUser.objects.filter(contract_id=contract_id, user_id=user.id, position__can_manage_contract_users=True)
    if not user_positions_in_contract.count() > 0 and not user.is_staff:
        return False
    return True


class ContractUserViewSet(mixins.RetrieveModelMixin,
        mixins.ListModelMixin,
        mixins.CreateModelMixin,
        mixins.DestroyModelMixin,
        PermissionByActionViewSet):
    queryset = models.ContractUser.objects.all().order_by('user__username')
    serializer_class = serializers.ContractUserSerializer

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
        'create': [permissions.IsAuthenticated,],
        'destroy': [permissions.IsAuthenticated,],
    }

    def get_queryset(self):
        contract_id = int(self.request.query_params.get('contract_id', False))
        user_positions_in_contract = models.ContractUser.objects.filter(contract_id=contract_id, user_id=self.request.user.id)
        if not user_positions_in_contract.count() > 0:
            raise PermissionDenied()
        queryset = models.ContractUser.objects.filter(contract_id=contract_id).order_by('user__username')
        return queryset

    def destroy(self, request, pk=None):
        contract_user = models.ContractUser.objects.get(pk=pk)
        if not is_user_can_manage_contract(contract_user.contract_id, self.request.user):
            raise PermissionDenied()
        if contract_user.user_id == self.request.user.id:
            raise exceptions.ParseError("Невозможно удалить себя из договора")

        contract_user.delete()
        return Response(content_type="text/plain", status=status.HTTP_200_OK)

    def create(self, request):
        contract_id = request.data['contract_id']
        if not is_user_can_manage_contract(contract_id, self.request.user):
            raise PermissionDenied()

        serializer = serializers.ContractUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, content_type="text/plain", status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AvailableContractUsersViewSet(mixins.ListModelMixin, PermissionByActionViewSet):
    queryset = models.UserCompany.objects.all().order_by('username')
    serializer_class = serializers.UserCompanySerializer

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
    }

    def get_queryset(self):
        contract_id = int(self.request.query_params.get('contract_id', False))
        if not is_user_can_manage_contract(contract_id, self.request.user):
            raise PermissionDenied()
        contract = models.Contract.objects.get(pk=contract_id)

        queryset = models.UserCompany.objects.filter(
            Q(company_type=models.content_type_id_by_content_string_type('organization'), company_id=contract.organization_id ) |
            Q(company_type=models.content_type_id_by_content_string_type('contractor'), company_id=contract.contractor_id )
        ).order_by('user__username')
        return queryset


class PositionViewSet(mixins.ListModelMixin, PermissionByActionViewSet):
    queryset = models.Position.objects.all().order_by('name')
    serializer_class = serializers.PositionSerializer

    permission_classes_by_action = {
        'list': [permissions.IsAuthenticated,],
        'retrieve': [permissions.IsAuthenticated,],
    }
