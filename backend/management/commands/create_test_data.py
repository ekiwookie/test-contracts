from random import randrange, choice

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from random_username.generate import generate_username

from backend import models


class Command(BaseCommand):
    organizations = (
        dict(name="Меркурий", attribute1="1", attribute2="6"),
        dict(name="Венера", attribute1="2", attribute2="7"),
        dict(name="Земля", attribute1="3", attribute2="8"),
        dict(name="Луна", attribute1="4", attribute2="9"),
        dict(name="Марс", attribute1="5", attribute2="10"),
    )
    contractors = (
        dict(name="Церера", attribute3="1", attribute4="6"),
        dict(name="Юпитер", attribute3="2", attribute4="7"),
        dict(name="Ио", attribute3="3", attribute4="8"),
        dict(name="Европа", attribute3="4", attribute4="9"),
        dict(name="Ганимед", attribute3="5", attribute4="10"),
    )
    users = (
        dict(username='ivan', email='ivan@example.com', password='test'),
        dict(username='vadim', email='vadim@example.com', password='test'),
        dict(username='nikodim', email='nikodim@example.com', password='test'),
        dict(username='prohor', email='nikodim@example.com', password='test'),
        dict(username='daria', email='daria@example.com', password='test'),
        dict(username='xenia', email='xenia@example.com', password='test'),
        dict(username='sofia', email='sofia@example.com', password='test'),
        dict(username='afdotia', email='afdotia@example.com', password='test'),
    )
    positions = (
        dict(name="Генеральный директор", can_manage_contract_users=True),
        dict(name="Владелец договора", can_manage_contract_users=True),
        dict(name="ЕОЛ"),
        dict(name="КИ"),
        dict(name="ДД"),
    )

    def handle(self, *args, **options):
        models.ContractUser.objects.all().delete()
        models.Contract.objects.all().delete()
        models.UserCompany.objects.all().delete()
        models.Organization.objects.all().delete()
        models.Contractor.objects.all().delete()
        get_user_model().objects.all().delete()


        for organization in self.organizations:
            models.Organization.objects.create(**organization)

        for contractor in self.contractors:
            models.Contractor.objects.create(**contractor)


        models.Position.objects.all().delete()
        for position in self.positions:
            models.Position.objects.create(**position)

        for user_data in self.users:
            user = get_user_model().objects.create_user(**user_data)

        for username in generate_username(50):
            user = get_user_model().objects.create_user(
                username=username,
                email=f'{username}@example.com',
                password='test'
            )

        for user in get_user_model().objects.all():
            random_company_type = choice([models.Organization, models.Contractor])
            if random_company_type == models.Organization:
                company = models.Organization.objects.order_by('?').first()
            else:
                company = models.Contractor.objects.order_by('?').first()
            models.UserCompany.objects.create(user=user, company=company)

        for i in range(40):
            random_organization =  models.Organization.objects.order_by('?').first()
            random_contractor =  models.Contractor.objects.order_by('?').first()
            contract = models.Contract.objects.create(organization=random_organization, contractor=random_contractor)

            random_company_users = get_user_model().objects.\
                filter(
                    Q(
                    company__company_type=ContentType.objects.get_for_model(models.Organization),
                    company__company_id=random_organization.id
                    )
                    |
                    Q(
                    company__company_type=ContentType.objects.get_for_model(models.Contractor),
                    company__company_id=random_contractor.id
                    )
                ).\
                order_by('?')

            random_count = randrange(random_company_users.count())

            for random_company_user in random_company_users:
                random_position =  models.Position.objects.order_by('?').first()

                models.ContractUser.objects.create(
                    contract=contract,
                    user=random_company_user,
                    position=random_position,
                )
                random_count -= 1
                if random_count == 0:
                    break
