import json

from rest_framework import serializers
from django.contrib.auth import get_user_model

from backend import models


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Position
        fields = ('id', 'name', 'can_manage_contract_users')

class UserCompanySerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField(read_only=True)
    name = serializers.SerializerMethodField(read_only=True)
    company_id = serializers.SerializerMethodField()
    user_id = serializers.SerializerMethodField()
    class Meta:
        model = models.UserCompany
        fields = (
            'username',
            'company_id',
            'user_id',
            'name',
            'company_type_string',
        )

    def get_username(self, obj):
        return obj.user.username

    def get_name(self, obj):
        return obj.company.name

    def get_company_id(self, obj):
        return obj.company.id

    def get_user_id(self, obj):
        return obj.user.id


class UserSerializer(serializers.ModelSerializer):
    company = UserCompanySerializer(read_only=True)
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'company')


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Organization
        fields = ('id', 'name', 'attribute1', 'attribute2')


class ContractorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Contractor
        fields = ('id', 'name', 'attribute3', 'attribute4')


class ContractSerializer(serializers.ModelSerializer):
    organization_id = serializers.PrimaryKeyRelatedField(
        source='organization',
        queryset=models.Organization.objects.all()
    )
    organization = OrganizationSerializer(read_only=True)
    contractor_id = serializers.PrimaryKeyRelatedField(
        source='contractor',
        queryset=models.Contractor.objects.all()
    )
    contractor = ContractorSerializer(read_only=True)

    class Meta:
        model = models.Contract
        fields = (
            'id',
            'organization_id',
            'organization',
            'contractor_id',
            'contractor',
            'concluded_on'
        )


class ContractUserSerializer(serializers.ModelSerializer):
    position_id = serializers.PrimaryKeyRelatedField(
        source='position',
        queryset=models.Position.objects.all()
    )
    position = PositionSerializer(read_only=True)
    user_id = serializers.PrimaryKeyRelatedField(
        source='user',
        queryset=get_user_model().objects.all()
    )
    user = UserSerializer(read_only=True)
    contract_id = serializers.PrimaryKeyRelatedField(
        source='contract',
        queryset=models.Contract.objects.all()
    )
    class Meta:
        model = models.ContractUser
        fields = (
            'id',
            'contract',
            'user',
            'user_id',
            'position',
            'position_id',
            'contract_id'
        )
        read_only_fields = (
            'contract',
        )

    def validate(self, data):
        count = models.ContractUser.objects.filter(
            position=data['position'],
            contract=data['contract'],
            user=data['user'],
        ).count()
        if count > 0:
            raise serializers.ValidationError({"user_id": "В договоре уже присутствует пользователь с такой должностью"})
        return data
