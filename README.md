# Тестовый проект по управлению контрактами

## Установка проекта
```
pip install -r requirements.txt
npm install
./manage.py migrate
```

## Тестовые данные
```
./manage.py create_test_data
```
Заполняет базу данных тестовыми данными.

Кроме пользователей ivan, vadim, nikodim, prohor, daria, xenia, sofia, afdotia с одинаковым паролем test, создает 50 произвольных пользователей.

## Запуск
```
npm run start
```
Тестовый сервер для клиентской части
