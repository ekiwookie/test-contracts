"""contracts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls import url
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import routers

from backend import views


api_router = routers.DefaultRouter()
api_router.register(r'users', views.UserViewSet)
api_router.register(r'organizations', views.OrganizationViewSet)
api_router.register(r'contractors', views.ContractorViewSet)
api_router.register(r'contracts', views.ContractViewSet)
api_router.register(r'contract_users', views.ContractUserViewSet)
api_router.register(r'available_contract_users', views.AvailableContractUsersViewSet)
api_router.register(r'positions', views.PositionViewSet)

urlpatterns = [
    path('api/token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    url(r'^api/', include((api_router.urls, "api"), namespace='v1-api')),
    path('', views.index_view)
]
